import React from "react";
import Link from "gatsby-link";

import github from "../img/github-icon.svg";
import logo from "../img/logo.svg";

const Navbar = () => (
  <nav className="navbar is-transparent">
    <div className="container">
      <div className="navbar-brand">
        <Link to="/" className="navbar-item" style={{ fontSize: "1.5rem" }}>
          Rose Lange
        </Link>
      </div>
      <div className="navbar-start">
        <Link className="navbar-item" activeClassName="is-active" to="/artwork">
          Artwork
        </Link>
        <Link className="navbar-item" activeClassName="is-active" to="/events">
          Events
        </Link>
        <Link className="navbar-item" activeClassName="is-active" to="/lessons">
          Lessons
        </Link>
        <Link className="navbar-item" activeClassName="is-active" to="/blog">
          Blog
        </Link>
        <Link className="navbar-item" activeClassName="is-active" to="/about">
          About
        </Link>
      </div>
    </div>
  </nav>
);

export default Navbar;
