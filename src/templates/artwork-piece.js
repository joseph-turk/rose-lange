import React from "react";
import PropTypes from "prop-types";
import Helmet from "react-helmet";

export const ArtworkPieceTemplate = ({
  description,
  title,
  helmet,
  price,
  image
}) => {
  return (
    <section className="section">
      {helmet || ""}
      <div className="container content">
        <div className="columns">
          <div className="column is-10 is-offset-1">
            <img src={image} alt={`Photo of ${title}`} />

            <h1 className="title is-size-2 has-text-weight-bold is-bold-light">
              {title} ({price})
            </h1>

            <p>{description}</p>
          </div>
        </div>
      </div>
    </section>
  );
};

ArtworkPieceTemplate.propTypes = {
  description: PropTypes.string,
  title: PropTypes.string,
  helmet: PropTypes.instanceOf(Helmet),
  image: PropTypes.string,
  price: PropTypes.number
};

const ArtworkPiece = ({ data }) => {
  const { markdownRemark: piece } = data;

  return (
    <ArtworkPieceTemplate
      description={piece.frontmatter.description}
      helmet={<Helmet title={`${piece.frontmatter.title} | Blog`} />}
      title={piece.frontmatter.title}
      image={piece.frontmatter.image}
      price={piece.frontmatter.price}
    />
  );
};

ArtworkPiece.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.object
  })
};

export default ArtworkPiece;

export const pageQuery = graphql`
  query ArtworkPieceByID($id: String!) {
    markdownRemark(id: { eq: $id }) {
      id
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        title
        description
        tags
      }
    }
  }
`;
